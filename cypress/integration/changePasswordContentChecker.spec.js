import Helper from "./helper/helper";

let home_label = '.MuiBox-root > .MuiButtonBase-root > .MuiFab-label > .MuiSvgIcon-root > path'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let background = 'body > .MuiDrawer-root > .MuiBackdrop-root'
let menu_changePass = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(6) > .MuiListItemText-root > .MuiTypography-root'
let oldPass = 'form > .MuiFormControl-root > .MuiFormControl-root > .MuiInputBase-root > #oldPassword'
let newPass = 'form > .MuiFormControl-root > .MuiFormControl-root > .MuiInputBase-root > #newPassword'
let saveBtn = '.MuiBox-root > form > .MuiFormControl-root > .MuiButtonBase-root > .MuiFab-label'

let helper = new Helper();

describe('Check change password after login', function () {

    helper.openOurGreatApp()
    helper.checkHomePage()
    helper.openMenu()
    helper.checkNotLoggedMenuContent()

    it('Open login page', function () {
        cy.get(menu_login).click()
        cy.get(background).click()
    });

    it('Check /login URI', function () {
        cy.url().should('include', '/login')
        cy.url().should('eq', 'http://localhost:9000/login')
    });

    helper.loginAs('test', 'test')

    it('Check if login was successfull', function () {
        cy.get(home_label).should('exist')
    });

    it('Check if login changes into logout', function () {
        cy.get(menu_button).click()
        cy.get(menu_login).should('exist')
        cy.get(menu_login).contains('Logout')
    })

    it('Open /user/changePassword', function () {
        cy.get(menu_changePass).click()
        cy.get(background).click()
    });

    helper.checkURI('/user/changePassword')

    it('Check content of /user/changePassword', function () {

        cy.get(oldPass).should('exist')
        cy.get(newPass).should('exist')
        cy.get(saveBtn).should('exist')

    });

})
