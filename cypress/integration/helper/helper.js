let home_priority = '.MuiGrid-root > .MuiFormControl-root > .MuiFormControl-root > .MuiInputBase-root > #priority'
let home_description = '.MuiGrid-root > .MuiGrid-root:nth-child(3) > .MuiFormControl-root > .MuiFormControl-root > .MuiInputBase-root'
let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let menu_home = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(1) > .MuiListItemText-root > .MuiTypography-root'
let menu_manage_inventory = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(2) > .MuiListItemText-root > .MuiTypography-root'
let menu_rooms = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(3) > .MuiListItemText-root > .MuiTypography-root'
let menu_users = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(4) > .MuiListItemText-root > .MuiTypography-root'
let menu_tickets = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(5) > .MuiListItemText-root > .MuiTypography-root'
let menu_change_pass = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(6) > .MuiListItemText-root > .MuiTypography-root'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let submit_button = '.MuiPaper-root > .MuiBox-root > form > .MuiButtonBase-root > .MuiButton-label'
let sign_in_username = '.MuiBox-root > form > .MuiFormControl-root > .MuiInputBase-root > #username'
let sign_in_pass = '.MuiBox-root > form > .MuiFormControl-root > .MuiInputBase-root > #password'

export default class Helper {

    openOurGreatApp() {
        it('Open our great app', function () {
            cy.viewport(1396, 657)
            cy.visit('http://localhost:9000/')
        })
    }

    checkHomePage() {
        it('Checks home page', function () {
            cy.get(home_priority).should('exist')
            cy.get(home_description).should('exist')
        })
    }

    openMenu() {
        it('Open Menu', function () {
            cy.get(menu_button).click()
        })
    }

    checkNotLoggedMenuContent() {
        it('Check menu content', function () {
            cy.get(menu_home).should('exist')
            cy.get(menu_manage_inventory).should('exist')
            cy.get(menu_rooms).should('exist')
            cy.get(menu_users).should('exist')
            cy.get(menu_tickets).should('exist')
            cy.get(menu_change_pass).should('exist')
            cy.get(menu_login).should('exist')

            cy.get(menu_home).contains('Create ticket')
            cy.get(menu_manage_inventory).contains('Manage inventory')
            cy.get(menu_rooms).contains('Rooms')
            cy.get(menu_users).contains('Users')
            cy.get(menu_tickets).contains('Tickets')
            cy.get(menu_change_pass).contains('Change password')
            cy.get(menu_login).contains('Login')
        })
    }

    checkURI(param) {
        it('Check ' + param + ' URI', function () {
            let link = 'http://localhost:9000' + param;
            cy.url().should('include', param)
            cy.url().should('eq', link)
        });
    }

    loginAs(user, pass) {
        it('Login as ' + user + '/' + pass, function () {
            cy.get(sign_in_username).click()
            cy.get(sign_in_username).type(user)

            cy.get(sign_in_pass).click()
            cy.get(sign_in_pass).type(pass)

            // submit
            cy.get(submit_button).click()
        });
    }
}
