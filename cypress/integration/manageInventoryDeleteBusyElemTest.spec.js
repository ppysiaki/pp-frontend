import Helper from "./helper/helper";

let home_label = '.MuiBox-root > .MuiButtonBase-root > .MuiFab-label > .MuiSvgIcon-root > path'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let background = 'body > .MuiDrawer-root > .MuiBackdrop-root'
let menu_manage_inventory = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(2) > .MuiListItemText-root > .MuiTypography-root'
let inventory_label = '.MuiContainer-root > div > div > .MuiBox-root > .MuiTypography-root'
let id = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(1)'
let name = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(2)'
let numOfElems = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(3)'
let Actions = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(4)'
let firstRow_elem1 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(1)'
let firstRow_elem2 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(2)'
let firstRow_elem3 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(3)'
let firstRow_elem4 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(4)'
let deleteBtn = '.MuiTableRow-root:nth-child(1) .MuiButtonBase-root:nth-child(2) path'
let alertForm = '#client-snackbar'

let helper = new Helper();

describe('Try to delete element of Manage inventory, which is connected with room.', function () {

    helper.openOurGreatApp()
    helper.checkHomePage()
    helper.openMenu()
    helper.checkNotLoggedMenuContent()

    it('Open login page', function () {
        cy.get(menu_login).click()
        cy.get(background).click()
    });

    it('Check /login URI', function () {
        cy.url().should('include', '/login')
        cy.url().should('eq', 'http://localhost:9000/login')
    });

    helper.loginAs('test', 'test')

    it('Check if login was successfull', function () {
        cy.get(home_label).should('exist')
    });

    it('Check if login changes into logout', function () {
        cy.get(menu_button).click()
        cy.get(menu_login).should('exist')
        cy.get(menu_login).contains('Logout')
    })

    it('Open manage inventory', function () {
        cy.get(menu_manage_inventory).click()
        cy.get(background).click()
    });

    helper.checkURI('/inventory')

    it('Check content of /inventory', function () {

        cy.get(inventory_label).should('exist')
        cy.get(id).should('exist')
        cy.get(name).should('exist')
        cy.get(numOfElems).should('exist')
        cy.get(Actions).should('exist')
        cy.get(firstRow_elem1).should('exist')
        cy.get(firstRow_elem2).should('exist')
        cy.get(firstRow_elem3).should('exist')
        cy.get(firstRow_elem4).should('exist')

        cy.get(inventory_label).contains('Inventory')
        cy.get(id).contains('Id')
        cy.get(name).contains('Name')
        cy.get(numOfElems).contains('Number of elements')
        cy.get(Actions).contains('Actions')
        cy.get(firstRow_elem1).contains('1')
        cy.get(firstRow_elem2).contains('PRO')
        cy.get(firstRow_elem3).contains('3')

    });

    it('Try to delete 1st element, check if it is impossible', function () {

        cy.get(deleteBtn).click()
        cy.get(alertForm).should('exist')
        cy.get(alertForm).contains('Cannot remove inventory which is assigned to a room.')

    });

})
