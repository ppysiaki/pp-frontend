import Helper from "./helper/helper";

let home_label = '.MuiBox-root > .MuiButtonBase-root > .MuiFab-label > .MuiSvgIcon-root > path'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let background = 'body > .MuiDrawer-root > .MuiBackdrop-root'
let menu_rooms = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(3) > .MuiListItemText-root > .MuiTypography-root'
let rooms_id = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(1)'
let rooms_label = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(2)'
let rooms_action = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(3)'
let room_list_el1 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(1)'
let room_list_el2 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(2)'
let room_list_el3 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(3)'
let editBtn = '.MuiTableBody-root > .MuiTableRow-root > .MuiTableCell-root > a:nth-child(1) > .MuiSvgIcon-root'
let editNameLabel = '.MuiGrid-root > .MuiGrid-root > .MuiFormControl-root > .MuiInputBase-root > #label'
let saveBtn = 'form'

let helper = new Helper();

describe('Edit room element', function () {

    helper.openOurGreatApp()
    helper.checkHomePage()
    helper.openMenu()
    helper.checkNotLoggedMenuContent()

    it('Open login page', function () {
        cy.get(menu_login).click()
        cy.get(background).click()
    });

    it('Check /login URI', function () {
        cy.url().should('include', '/login')
        cy.url().should('eq', 'http://localhost:9000/login')
    });

    helper.loginAs('test', 'test')

    it('Check if login was successfull', function () {
        cy.get(home_label).should('exist')
    });

    it('Check if login changes into logout', function () {
        cy.get(menu_button).click()
        cy.get(menu_login).should('exist')
        cy.get(menu_login).contains('Logout')
    })

    it('Open /rooms', function () {
        cy.get(menu_rooms).click()
        cy.get(background).click()
    });

    helper.checkURI('/rooms')

    it('Check content of /rooms', function () {
        cy.get(rooms_id).should('exist')
        cy.get(rooms_label).should('exist')
        cy.get(rooms_action).should('exist')
        cy.get(room_list_el1).should('exist')
        cy.get(room_list_el2).should('exist')
        cy.get(room_list_el3).should('exist')

        cy.get(rooms_id).contains('Id')
        cy.get(rooms_label).contains('Label')
        cy.get(rooms_action).contains('Actions')
        cy.get(room_list_el1).contains('1')
        cy.get(room_list_el2).contains('106')
    });

    it('Edit element, check changes, undo changes, check', function () {
        cy.get(editBtn).click()
        cy.get(editNameLabel).click()
        cy.get(editNameLabel).type('1')
        cy.get(saveBtn).submit()
        cy.get(room_list_el2).contains('1')

        cy.get(editBtn).click()
        cy.get(editNameLabel).click()
        cy.get(editNameLabel).type('106')
        cy.get(saveBtn).submit()
        cy.get(room_list_el2).contains('106')

    });

})
