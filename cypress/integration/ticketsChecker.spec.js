import Helper from "./helper/helper";

let home_label = '.MuiBox-root > .MuiButtonBase-root > .MuiFab-label > .MuiSvgIcon-root > path'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let background = 'body > .MuiDrawer-root > .MuiBackdrop-root'
let menu_tickets = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(5) > .MuiListItemText-root > .MuiTypography-root'

let helper = new Helper();

describe('Check Tickets after login', function () {

    helper.openOurGreatApp()
    helper.checkHomePage()
    helper.openMenu()
    helper.checkNotLoggedMenuContent()

    it('Open login page', function () {
        cy.get(menu_login).click()
        cy.get(background).click()
    });

    it('Check /login URI', function () {
        cy.url().should('include', '/login')
        cy.url().should('eq', 'http://localhost:9000/login')
    });

    helper.loginAs('test', 'test')

    it('Check if login was successfull', function () {
        cy.get(home_label).should('exist')
    });

    it('Check if login changes into logout', function () {
        cy.get(menu_button).click()
        cy.get(menu_login).should('exist')
        cy.get(menu_login).contains('Logout')
    })

    it('Open Tickets', function () {
        cy.get(menu_tickets).click()
        cy.get(background).click()
    });

    helper.checkURI('/tickets')

    it('Check content of /tickets', function () {
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(1) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(2) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(3) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(4) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(5) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(6) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(7) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(8) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(9) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(10) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(11) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(12) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(13) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(1) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(2) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(3) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(4) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(5) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(6) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(8) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(11) > span').should('exist')
        cy.get('.MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(12) > span').should('exist')
    });

})


