import Helper from "./helper/helper";

let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let menu_manage_inventory = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(2) > .MuiListItemText-root > .MuiTypography-root'
let menu_rooms = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(3) > .MuiListItemText-root > .MuiTypography-root'
let menu_users = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(4) > .MuiListItemText-root > .MuiTypography-root'
let menu_tickets = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(5) > .MuiListItemText-root > .MuiTypography-root'
let menu_change_pass = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(6) > .MuiListItemText-root > .MuiTypography-root'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let sign_in_background = 'body > .MuiDrawer-root > .MuiBackdrop-root'
let sign_in_pass = '.MuiBox-root > form > .MuiFormControl-root > .MuiInputBase-root > #password'
let sign_in_button = '.MuiContainer-root > .MuiContainer-root > .MuiPaper-root > .MuiBox-root > form'

let helper = new Helper()

function setFocusOnMP() {
    it('Set focus on Main Page and check if login form exists', function () {
        cy.get(sign_in_background).click()
        cy.get(sign_in_button).should('exist')
        cy.get(sign_in_pass).should('exist')
    })
}

describe('Checks if unauthorized user can do anything', function () {

    helper.openOurGreatApp()
    helper.checkHomePage()
    helper.openMenu()
    helper.checkNotLoggedMenuContent()

    it('Open Manage inventory', function () {
        cy.get(menu_manage_inventory).click()
    })

    setFocusOnMP()
    helper.checkURI('/login')

    it('Open Rooms', function () {
        cy.get(menu_button).click()
        cy.get(menu_rooms).click()
    })

    setFocusOnMP()
    helper.checkURI('/login')

    it('Open Users', function () {
        cy.get(menu_button).click()
        cy.get(menu_users).click()
    })

    setFocusOnMP()
    helper.checkURI('/login')

    it('Open Tickets', function () {
        cy.get(menu_button).click()
        cy.get(menu_tickets).click()
    })

    setFocusOnMP()
    helper.checkURI('/login')

    it('Open Check password', function () {
        cy.get(menu_button).click()
        cy.get(menu_change_pass).click()
    })

    setFocusOnMP()
    helper.checkURI('/login')

    it('Open Login', function () {
        cy.get(menu_button).click()
        cy.get(menu_login).click()
    })

    setFocusOnMP()
    helper.checkURI('/login')

})
