import Helper from "./helper/helper";

let home_label = '.MuiBox-root > .MuiButtonBase-root > .MuiFab-label > .MuiSvgIcon-root > path'
let menu_login = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(7) > .MuiListItemText-root > .MuiTypography-root'
let menu_button = '.MuiPaper-root > .MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root'
let background = 'body > .MuiDrawer-root > .MuiBackdrop-root'
let menu_users = '.menuList--3FD6a > .MuiList-root > .MuiButtonBase-root:nth-child(4) > .MuiListItemText-root > .MuiTypography-root'
let users_id = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(1)'
let users_username = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(2)'
let users_action = '.MuiTableContainer-root > .MuiTable-root > .MuiTableHead-root > .MuiTableRow-root > .MuiTableCell-root:nth-child(3)'
let first_row_el1 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(1)'
let first_row_el2 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(2)'
let first_row_el3 = '.MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > .MuiTableRow-root:nth-child(1) > .MuiTableCell-root:nth-child(3)'

let helper = new Helper();

describe('Check Users after login', function () {

    helper.openOurGreatApp()
    helper.checkHomePage()
    helper.openMenu()
    helper.checkNotLoggedMenuContent()

    it('Open login page', function () {
        cy.get(menu_login).click()
        cy.get(background).click()
    });

    it('Check /login URI', function () {
        cy.url().should('include', '/login')
        cy.url().should('eq', 'http://localhost:9000/login')
    });

    helper.loginAs('test', 'test')

    it('Check if login was successfull', function () {
        cy.get(home_label).should('exist')
    });

    it('Check if login changes into logout', function () {
        cy.get(menu_button).click()
        cy.get(menu_login).should('exist')
        cy.get(menu_login).contains('Logout')
    })

    it('Open /user', function () {
        cy.get(menu_users).click()
        cy.get(background).click()
    });

    helper.checkURI('/user')

    it('Check content of /user', function () {
        cy.get(users_id).should('exist')
        cy.get(users_username).should('exist')
        cy.get(users_action).should('exist')
        cy.get(first_row_el1).should('exist')
        cy.get(first_row_el2).should('exist')
        cy.get(first_row_el3).should('exist')

        cy.get(users_id).contains('Id')
        cy.get(users_username).contains('Username')
        cy.get(users_action).contains('Actions')
        cy.get(first_row_el1).contains('1')
        cy.get(first_row_el2).contains('test')
    });

})
