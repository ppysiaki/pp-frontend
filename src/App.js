import React, {Component} from "react";
import AppMenu from "./components/app-menu/appMenu";
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import {Container, Drawer, Toolbar} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from '@material-ui/icons/Menu';
import Typography from "@material-ui/core/Typography";
import Login from "./components/user/login";
import InventoryComponent from "./components/inventory/InventoryComponent";
import TicketComponent from "./components/tickets/TicketComponent";
import PrivateRoute from "./components/shared/PrivateRoute";
import UserComponent from "./components/user/UserComponent";
import RoomComponent from "./components/room/RoomComponent";
import ReportComponent from "./components/report/ReportComponent";

class App extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            drawer: false,
            title: "Hotel Ticket Management"
        };

        this.toggleDrawer = this.toggleDrawer.bind(this);
    }

    toggleDrawer() {
        this.setState({
            drawer: !this.state.drawer
        });
    }

    render() {
        return (
            <Router>
                <Drawer open={this.state.drawer} onClose={this.toggleDrawer}>
                    <AppMenu/>
                </Drawer>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" onClick={this.toggleDrawer}>
                            <MenuIcon style={{fill: 'white'}}/>
                        </IconButton>

                        <Typography variant={"h6"}>{this.state.title}</Typography>
                    </Toolbar>
                </AppBar>
                <Container>
                    <Switch>
                        <Route path="/" exact component={ReportComponent} />
                        <Route path="/login" component={Login} />
                        <PrivateRoute path="/rooms" component={RoomComponent} />
                        <PrivateRoute path="/inventory" component={InventoryComponent} />
                        <PrivateRoute path="/user" component={UserComponent} />
                        <PrivateRoute path="/tickets" component={TicketComponent} />
                        <PrivateRoute path="/user/changePassword" component={UserComponent} />
                    </Switch>
                </Container>
            </Router>
        );
    }
}

export default App;