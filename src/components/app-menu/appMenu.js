import React from "react";
import style from "./style.scss"
import {Link} from "react-router-dom";
import {ListItemIcon, ListItemText, MenuList} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import SettingsIcon from '@material-ui/icons/Settings';
import PersonIcon from '@material-ui/icons/Person';
import {USER_ACTIONS} from "../../redux/actions/Authentication";
import {connect} from "react-redux";
import {VpnKey} from "@material-ui/icons";
import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber';
import {MeetingRoom} from "@material-ui/icons";
import {PeopleAlt} from "@material-ui/icons";


const AppMenu = ({logout, currentUser}) => {
    const logoutHandler = () => {
        logout();
    }

    return (
        <div className={style.menuList}>
            <MenuList>
                <MenuItem component={Link} to={"/"}>
                    <ListItemIcon><ConfirmationNumberIcon/></ListItemIcon>
                    <ListItemText>Create ticket</ListItemText>
                </MenuItem>
                <MenuItem component={Link} to={"/inventory"}>
                    <ListItemIcon><SettingsIcon/></ListItemIcon>
                    <ListItemText>Manage inventory</ListItemText>
                </MenuItem>
                <MenuItem component={Link} to={"/rooms"}>
                    <ListItemIcon><MeetingRoom/></ListItemIcon>
                    <ListItemText>Rooms</ListItemText>
                </MenuItem>
                <MenuItem component={Link} to={"/user"}>
                    <ListItemIcon><PeopleAlt/></ListItemIcon>
                    <ListItemText>Users</ListItemText>
                </MenuItem>
                <MenuItem component={Link} to={"/tickets"}>
                    <ListItemIcon><ConfirmationNumberIcon/></ListItemIcon>
                    <ListItemText>Tickets</ListItemText>
                </MenuItem>
                <MenuItem component={Link} to={"/user/changePassword"}>
                    <ListItemIcon><VpnKey/></ListItemIcon>
                    <ListItemText>Change password</ListItemText>
                </MenuItem>
                {currentUser ? (
                    <MenuItem component={Link} to={"#"} onClick={logoutHandler}>
                        <ListItemIcon><PersonIcon/></ListItemIcon>
                        <ListItemText>Logout</ListItemText>
                    </MenuItem>
                ) : (
                    <MenuItem component={Link} to={"/login"}>
                        <ListItemIcon><PersonIcon/></ListItemIcon>
                        <ListItemText>Login</ListItemText>
                    </MenuItem>
                )}
            </MenuList>
        </div>
    );
}

const mapStateToProps = store => ({
    currentUser: store.Authentication.user
});

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(USER_ACTIONS.logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(AppMenu);