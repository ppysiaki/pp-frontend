import React, {useEffect, useState} from "react";
import {Route, Switch} from "react-router-dom"
import InventoryList from "./InventoryList";
import InventoryElementEdit from "./InventoryElementEdit";
import {connect} from "react-redux";
import {clearError} from "../../redux/actions/Inventory"
import InventoryCreate from "./InventoryCreate";
import {useSnackbar} from "notistack";

const InventoryComponent = ({data, clearError, errors}) => {
    const [selected, setSelected] = useState(null);
    const {enqueueSnackbar} = useSnackbar();


    useEffect(() => {
        if (errors) {
            enqueueSnackbar(errors.error, {
                variant: "error",
                onClose: clearError
            });
        }
    }, [errors]);

    return <div>
        <Switch>
            <Route path={"/inventory/create"}
                   render={props => <InventoryCreate {...props}/>}/>
            <Route path="/inventory/:id/edit"
                   render={props => <InventoryElementEdit {...props} selected={selected}/>}/>
            <Route path="/inventory"
                   render={props => <InventoryList {...props} data={data} onSelect={setSelected}/>}/>
        </Switch>
    </div>
}

const mapStateToProps = state => ({
    data: state.Inventory.inventories,
    errors: state.Inventory.errors
});

const mapDispatchToProps = dispatch => ({
    clearError: () => dispatch(clearError())
});

export default connect(mapStateToProps, mapDispatchToProps)(InventoryComponent);