import React, {useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom";
import {useSnackbar} from "notistack";
import {Box} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Inventory from "./models/Inventory";
import {
    createInventory,
    linkInventoryElement,
    loadInventoryElements
} from "../../redux/actions/Inventory";
import {connect} from "react-redux";
import {makeStyles} from "@material-ui/core/styles";
import InventoryForm from "./InventoryForm";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
}));

const InventoryCreate = ({createInventory, inventoryElements, loadElements, linkItem}) => {
    const classes = useStyles();
    const [pageTitle] = useState("Inventory");
    const history = useHistory();
    const {enqueueSnackbar} = useSnackbar();

    useEffect(() => {
        if (inventoryElements.length === 0) {
            loadElements();
        }
    }, []);

    const handleSubmit = ({selectedItem}) => {
        createInventory(selectedItem)
            .then(() => enqueueSnackbar("Inventory saved", {
                variant: "success"
            }))
            .then(() => selectedItem.inventoryElementList.forEach(({inventory, inventoryElement}) => linkItem(inventory, inventoryElement)))
            .then(() => history.goBack())
            .catch(() => enqueueSnackbar("Error saving", {
                variant: "error"
            }));
    };

    return (
        <div>
            <Box m={2}>
                <Typography variant={"h2"}><span>{pageTitle} - Create</span></Typography>
                <Typography>
                    <Link to={"/inventory"} className={classes.alignCenter}>
                        <ArrowBackIcon/> Go back
                    </Link>
                </Typography>
            </Box>
            <InventoryForm selected={new Inventory()} create={true} handleSubmit={handleSubmit}/>
        </div>
    )
}

const mapStateToProps = store => ({
    inventoryElements: store.Inventory.inventoryElements
});

const mapDispatchToProps = dispatch => ({
    createInventory: (inventory) => dispatch(createInventory(inventory)),
    loadElements: () => dispatch(loadInventoryElements()),
    linkItem: (inventory, inventoryElement) => dispatch(linkInventoryElement(inventory, inventoryElement))
});

export default connect(mapStateToProps, mapDispatchToProps)(InventoryCreate);