import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import PropTypes from 'prop-types';
import {Box} from "@material-ui/core";
import {Link} from "react-router-dom";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Inventory from "./models/Inventory";
import {useSnackbar} from "notistack";
import {
    assignInventoryElement,
    linkInventoryElement, loadInventoryElements,
    unlinkInventoryElement,
    updateInventory
} from "../../redux/actions/Inventory";
import {connect} from "react-redux";
import InventoryForm from "./InventoryForm";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    },
    broken: {
        color: '#f00'
    },
    ok: {
        color: '#197d00'
    },
    disabled: {
        opacity: 0.4
    }
}));

const InventoryElementEdit = ({selected, unlinkItem, updateInventory, inventoryElements, loadElements, linkItem}) => {
    const classes = useStyles();
    const [pageTitle] = useState("Inventory");
    const history = useHistory();
    const {enqueueSnackbar} = useSnackbar();

    useEffect(() => {
        if (inventoryElements.length === 0) {
            loadElements();
        }
    }, []);

    const handleSubmit = ({selectedItem, link, unlink}) => {
        updateInventory(selectedItem)
            .then(() => enqueueSnackbar("Inventory saved", {
                variant: "success"
            }))
            .then(() => unlink.forEach(({inventory, inventoryElement}) => unlinkItem(inventory, inventoryElement)))
            .then(() => link.forEach(({inventory, inventoryElement}) => linkItem(inventory, inventoryElement)))
            .then(() => history.goBack())
            .catch(() => enqueueSnackbar("Error saving", {
                variant: "error"
            }));
    };

    return (
        <div>
            <Box m={2}>
                <Typography variant={"h2"}><span>{pageTitle} - Edit</span></Typography>
                <Typography>
                    <Link to={"/inventory"} className={classes.alignCenter}>
                        <ArrowBackIcon/> Go back
                    </Link>
                </Typography>
            </Box>
            <InventoryForm selected={selected} handleSubmit={handleSubmit}/>
        </div>
    )
}

InventoryElementEdit.propTypes = {
    onSubmit: PropTypes.func,
    selected: PropTypes.instanceOf(Inventory)
}

const mapStateToProps = store => ({
    inventoryElements: store.Inventory.inventoryElements
});

const mapDispatchToProps = dispatch => ({
    assignElement: (inventory, element) => dispatch(assignInventoryElement(inventory, element)),
    loadElements: () => dispatch(loadInventoryElements()),
    updateInventory: inventory => dispatch(updateInventory(inventory)),
    linkItem: (inventory, inventoryElement) => dispatch(linkInventoryElement(inventory, inventoryElement)),
    unlinkItem: (inventory, inventoryElement) => dispatch(unlinkInventoryElement(inventory, inventoryElement))
});

export default connect(mapStateToProps, mapDispatchToProps)(InventoryElementEdit);