import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    broken: {
        color: '#f00'
    },
    ok: {
        color: '#197d00'
    },
}));
const InventoryElementList = ({selectedItem, rowClass, actionsBuilder}) => {
    const classes = useStyles();

    const isBrokenDisplay = isBroken => isBroken ? <strong className={classes.broken}>Broken</strong> :
        <strong className={classes.ok}>OK</strong>;
    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell><span>Id</span></TableCell>
                        <TableCell><span>Name</span></TableCell>
                        <TableCell><span>Status</span></TableCell>
                        {
                            actionsBuilder ? (<TableCell><span>Actions</span></TableCell>) : ''
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {selectedItem?.inventoryElementList.map((row, i) => {
                        return (
                            <TableRow key={i} className={rowClass ? rowClass(row) : ''}>
                                <TableCell><span>{row.id}</span></TableCell>
                                <TableCell><span>{row.name}</span></TableCell>
                                <TableCell><span>{isBrokenDisplay(row.isBroken)}</span></TableCell>
                                {
                                    actionsBuilder ? (<TableCell>{actionsBuilder(row)}</TableCell>) : ''
                                }
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default InventoryElementList;