import {Box, Grid, Paper} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import FormGroup from "@material-ui/core/FormGroup";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import LinkIcon from "@material-ui/icons/Link";
import LinkOffIcon from "@material-ui/icons/LinkOff";
import React, {useState} from "react";
import {connect} from "react-redux";
import {assignInventoryElement} from "../../redux/actions/Inventory";
import {makeStyles} from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import InventoryElementList from "./InventoryElementList";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    disabled: {
        opacity: 0.4
    },
    flexEnd: {
        display: "flex",
        justifyContent: "flex-end"
    }
}));

const InventoryForm = ({handleSubmit, create = false, selected, inventoryElements, assignElement}) => {
    const classes = useStyles();
    const [selectedItem, setSelectedItem] = useState(selected);
    const [unlink, setUnlinked] = useState([]);
    const [link, setLinked] = useState([]);

    const handleChange = e => {
        const {name, value} = e.target;
        setSelectedItem({...selectedItem, [name]: value});
    };

    const addLink = (inventory, inventoryElement) => {
        const linked = Array.from(link);
        linked.push(({inventory, inventoryElement}));

        setLinked(linked);

        if (create) {
            selectedItem.inventoryElementList.push(inventoryElement);
        }
    }

    const toggleUnlink = (inventory, inventoryElement) => {
        const newUnlink = Array.from(unlink);
        const newObject = {inventory, inventoryElement};
        if (isElementQueuedForUnlink(inventory, inventoryElement)) {
            const index = newUnlink.findIndex((data) => data.inventoryElement.id === inventoryElement.id);
            newUnlink.splice(index, 1);
        } else {
            newUnlink.push(newObject);
        }

        setUnlinked(newUnlink);
    }

    const isElementQueuedForUnlink = (inventory, inventoryElement) => !!unlink.find(item => item.inventoryElement.id === inventoryElement.id);

    const handleAddAnother = (item) => {
        addLink(selectedItem, item);
        assignElement(selectedItem, item);
    };

    const rowActionBuilder = (row) => {
        return (
            <div>
                <Button onClick={() => toggleUnlink(selectedItem, row)}>
                    {isElementQueuedForUnlink(selectedItem, row) ?
                        <LinkIcon/> :
                        <LinkOffIcon/>}
                </Button>
            </div>);
    };
    const rowClass = (row) => isElementQueuedForUnlink(selectedItem, row) ? classes.disabled : null;

    const submit = (e) => {
        e.preventDefault();
        handleSubmit({selectedItem, link, unlink});
    }

    return (
        <Paper elevation={1}>
            <form onSubmit={submit}>
                <Box p={2} className={classes.flexEnd}>
                    <Fab variant={"extended"} size={"small"} type={"submit"}>
                        <SaveIcon/>{' '}Save
                    </Fab>
                </Box>
                <Box p={2}>
                    <FormGroup>
                        <FormControl>
                            <TextField id="name" label="Name" variant="outlined"
                                       name={"symbol"}
                                       value={selectedItem?.symbol}
                                       onChange={handleChange}/>
                        </FormControl>
                    </FormGroup>
                </Box>
                <Divider/>
                {!create ? (
                    <div>
                        <Box p={2}>
                            <Grid container>
                                <Grid item xs={10}>
                                    <Typography component={"h2"}>Items</Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <FormControl>
                                        <Select
                                            displayEmpty
                                            value={""}>
                                            <MenuItem key={0} value={""}>- Add another -</MenuItem>
                                            {
                                                inventoryElements?.map(item => (
                                                    <MenuItem key={item.id} value={item.id}
                                                              onClick={() => handleAddAnother(item)}>{item.name}</MenuItem>
                                                ))
                                            }
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Box>
                        {selectedItem?.inventoryElementList.length > 0 && (
                            <InventoryElementList actionsBuilder={rowActionBuilder} rowClass={rowClass}
                                                  selectedItem={selectedItem}/>
                        )}
                    </div>
                ) : ''}
            </form>
        </Paper>
    );
};

const mapStateToProps = store => ({
    inventoryElements: store.Inventory.inventoryElements
});

const mapDispatchToProps = dispatch => ({
    assignElement: (inventory, element) => dispatch(assignInventoryElement(inventory, element)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InventoryForm);