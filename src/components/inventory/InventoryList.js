import React, {useEffect, useState} from "react";
import {Paper, Table, TableBody, TableCell, TableHead, TableContainer, TableRow} from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {fetchInventories, removeInventory} from "../../redux/actions/Inventory";
import {connect} from "react-redux";
import {useSnackbar} from "notistack";

const useRowStyles = makeStyles({
    alignRight: {
        display: "flex",
        justifyContent: "flex-end"
    },
});

const InventoryList = ({data, onSelect, removeInventory, fetchInventories}) => {
    const classes = useRowStyles();
    const [pageTitle] = useState("Inventory");
    const itemEditPath = inventoryElement => `/inventory/${inventoryElement.id}/edit`;
    const itemCreatePath = _ => `/inventory/create`;
    const {enqueueSnackbar} = useSnackbar();

    useEffect(() => {
        fetchInventories()
    }, []);

    const removeRow = row => {
        removeInventory(row)
            .then(() => enqueueSnackbar("Item removed", {
                variant: "success"
            }))
            .catch(e => {});
    }

    return <div>
        <Box m={2}>
            <Typography variant={"h2"}>{pageTitle}</Typography>
        </Box>
        <Paper elevation={1}>
            <Box p={2} className={classes.alignRight}>
                <ButtonGroup size={"small"}>
                    <Button component={Link} to={itemCreatePath()}><AddIcon/>{" "}Add new</Button>
                </ButtonGroup>
            </Box>
            <Divider/>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                Id
                            </TableCell>
                            <TableCell>
                                Name
                            </TableCell>
                            <TableCell>
                                Number of elements
                            </TableCell>
                            <TableCell>
                                Actions
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row, i) => {
                            return (
                                <TableRow key={i}>
                                    <TableCell>{row.id}</TableCell>
                                    <TableCell>{row.symbol}</TableCell>
                                    <TableCell>{row.inventoryElementList.length}</TableCell>
                                    <TableCell>
                                        <Button component={Link} to={itemEditPath(row)}
                                                onClick={() => onSelect(row)}><EditIcon/></Button>
                                        <Button onClick={() => removeRow(row)}><DeleteIcon/></Button>
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    </div>;
}

const mapStateToProps = state => ({
    data: state.Inventory.inventories
});

const mapDispatchToProps = dispatch => ({
    fetchInventories: () => dispatch(fetchInventories()),
    removeInventory: inventory => dispatch(removeInventory(inventory))
});

export default connect(mapStateToProps, mapDispatchToProps)(InventoryList);