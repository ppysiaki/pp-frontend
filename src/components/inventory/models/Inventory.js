class Inventory {
    id = null;
    symbol = "";
    inventoryElementList = [];

    constructor(id = null, symbol = "", inventoryElements = []) {
        this.id = id;
        this.symbol = symbol;
        this.inventoryElementList = inventoryElements;
    }
}

export default Inventory;