class InventoryElement {
    id;
    name;
    isBroken;

    constructor(id, name, isBroken) {
        this.id = id;
        this.name = name;
        this.isBroken = isBroken;
    }
}

export default InventoryElement;