import {Box, Paper} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import FormControl from "@material-ui/core/FormControl";
import React, {useEffect, useState} from "react";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {ROOM_ACTION} from "../../redux/actions/room";
import {connect} from "react-redux";
import Room from "../room/model/Room";
import {createTicket} from "../../redux/actions/Tickets";
import Ticket from "../tickets/model/Ticket";
import TextField from "@material-ui/core/TextField";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import {useSnackbar} from "notistack";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";

const ReportComponent = ({rooms, loadRooms, createTicket}) => {
    const [selectedRoom, setSelectedRoom] = useState('');
    const [selectedInventoryElement, setSelectedInventoryElement] = useState('');
    const [newTicketData, setNewTicketData] = useState(null)
    const {enqueueSnackbar} = useSnackbar();

    useEffect(() => {
        loadRooms();
    }, [])

    const selectRoom = (room) => {
        setSelectedRoom(room.label);
        setNewTicketData({...newTicketData, room: room});
        setSelectedInventoryElement(null);
    }

    const selectInventoryElement = (element) => {
        setSelectedInventoryElement(element.name);
        setNewTicketData({...newTicketData, inventoryElement: element});
    }

    const handleChange = e => {
        const {name, value} = e.target;
        setNewTicketData({...newTicketData, [name]: value});
    };

    const handleSubmit = e => {
        e.preventDefault();
        let ticket = Object.assign(new Ticket(), newTicketData);
        createTicket(ticket)
            .then(() => {
                enqueueSnackbar("Ticket created", {
                    variant: "success",
                });

                setSelectedRoom(null);
                setNewTicketData(null);
                setSelectedInventoryElement(null);
            })
            .catch(error => enqueueSnackbar(error, {
                variant: "error",
            }));
    }

    return <div>
        <form onSubmit={handleSubmit}>
            <Box m={2}>
                <Typography variant={"h2"}><span>Create ticket - {selectedRoom}</span></Typography>
                    <Fab variant={"extended"} size={"small"} type={"submit"}>
                        <SaveIcon/>{' '}Create
                    </Fab>
            </Box>
            <Paper elevation={1}>
                <Box p={2}>
                    <Grid container justify={'center'} alignContent={'center'} spacing={2}>
                        <Grid item xs={4}>
                            <Grid container justify={'center'} alignContent={'center'} spacing={2}>
                                <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="rooms ">Room</InputLabel>
                        <Select
                            labelid={'rooms'}
                            id={'rooms'}
                            label={'Rooms'}
                            name={'room'}
                            value={selectedRoom || ''}>
                            {rooms?.map((row, i) => {
                                return <MenuItem key={row.id} onClick={() => selectRoom(row)} value={row.label}>{row.label}</MenuItem>
                            })}
                        </Select>

                    </FormControl>
                                </Grid>
                    {selectedRoom !== null ?
                        <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="inventoryElement ">Element</InputLabel>
                        <Select
                            labelid={'inventoryElement'}
                            id={'inventoryElement'}
                            displayEmpty={false}
                            label={'inventoryElement'}
                            name={'inventoryElement'}
                            value={selectedInventoryElement || ''}>
                            {newTicketData?.room?.inventory?.inventoryElementList?.map((row, i) => {
                                return <MenuItem key={row.id} value={row.name} onClick={() => selectInventoryElement(row)}>{row.name}</MenuItem>
                            })}

                        </Select>
                    </FormControl>
                        </Grid>
                    : ''}
                        </Grid>
                        </Grid>
                            <Grid item xs={2}>
                    <FormControl >
                       <TextField id="priority" label="priority" variant="outlined"
                                  name={"priority"}
                                  placeholder={'priority'}
                                  required={true}
                                  type={'number'}
                                  value={newTicketData?.priority || ''}
                                  onChange={handleChange}/>
                    </FormControl>
                            </Grid>
                                <Grid item xs={6}>
                    <FormControl fullWidth>
                      <TextField id="description" label="description" variant="outlined"
                              name={"description"}
                              placeholder={'Description'}
                              required={true}
                              multiline
                              value={newTicketData?.description || ''}
                              onChange={handleChange}/>
                    </FormControl>
                                </Grid>
                    </Grid>
                </Box>
            </Paper>
        </form>
    </div>

}

const mapStateToProps = state => ({
    rooms: state.Room.rooms,
    basicRoom: Room.createWithLabel(''),
});

const mapDispatchToProps = (dispatch, props) => ({
    loadRooms: _ => dispatch(ROOM_ACTION.loadRooms()),
    createTicket: (ticket) => dispatch(createTicket(ticket))
});


export default connect(mapStateToProps, mapDispatchToProps)(ReportComponent);