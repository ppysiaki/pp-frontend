import React, {useState } from "react";
import {Route, Switch} from "react-router-dom"
import RoomsList from "./RoomsList";
import RoomEdit from "./RoomEdit";
import {ROOM_ACTION} from "../../redux/actions/room";
import {connect} from "react-redux";
import RoomCreate from "./RoomCreate";
import Room from "./model/Room";
import {fetchInventories} from "../../redux/actions/Inventory";

const RoomComponent = ({updateRoom, createRoom, fetchInventories}) => {
    return <div>
        <Switch>
            <Route  path="/rooms/create"
                    render={props => <RoomCreate {...props} room={Room.createEmpty()} title={'Create room'} onSubmit={createRoom} fetchInventories={fetchInventories}/>}
            />
            <Route  path="/rooms/:id/edit"
                    render={props => <RoomEdit {...props} title={'Edit room'} onSubmit={updateRoom} fetchInventories={fetchInventories}/>}
            />
            <Route  path="/rooms"
                    render={props => <RoomsList {...props} />}
            />
        </Switch>
    </div>

}

const mapDispatchToProps = dispatch => ({
    updateRoom: room => dispatch(ROOM_ACTION.updateRoom(room)),
    createRoom: room => dispatch(ROOM_ACTION.createRoom(room)),
    fetchInventories: () => dispatch(fetchInventories()),
});

export default connect(null, mapDispatchToProps)(RoomComponent);