import React, {useEffect, useMemo, useState} from "react";
import Typography from "@material-ui/core/Typography";
import {Box, Paper} from "@material-ui/core";
import {Link, useHistory} from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import {makeStyles} from "@material-ui/core/styles";
import {connect} from "react-redux";
import {ROOM_ACTION} from "../../redux/actions/room";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
}));

const RoomEdit = ({room, inventories, title, loadRoom, onSubmit, fetchInventories}) => {

    const errorHandler = null;


    const [selectedRoom, setSelectedRoom] = useState(room);
    const [error, setError] = useState(errorHandler);
    useEffect(() => {
        loadRoom();
        fetchInventories();
    }, []);

    useMemo(() => {
        setSelectedRoom(room);
    }, [room]);


    const classes = useStyles();
    const history = useHistory();

    const handleSubmit = e => {
        e.preventDefault();
        onSubmit(selectedRoom)
            .then( _ => history.goBack() )
            .catch( errorMsg => {console.log(errorMsg); setError(errorMsg)});
    }

    const handleChange = e => {
        const {name, value} = e.target;
        setSelectedRoom({...selectedRoom, [name]: value});
    };

    const handleInventoryChange = e => {
        const {name, value} = e.target;

        const inventory = inventories.find((inventory => inventory.id === value));

        setSelectedRoom({...selectedRoom, ['inventory']: inventory});
    }
    return  <div>
        <form onSubmit={handleSubmit}>
            <Box m={2}>
                <Typography variant={"h2"}><span>{title} - {selectedRoom?.label}</span></Typography>
                <Typography variant={"h6"}><span>{error}</span></Typography>
                <div className={classes.flexEvenly}>
                    <Typography>
                        <Link to={"/rooms"} className={classes.alignCenter}>
                            <ArrowBackIcon/> Go back
                        </Link>
                    </Typography>
                    <Fab variant={"extended"} size={"small"} type={"submit"}>
                        <SaveIcon/>{' '}Save
                    </Fab>
                </div>
            </Box>
            <Paper elevation={1}>
                <Box p={2}>
                    <FormControl fullWidth>
                         <Grid container direction={'row'} spacing={3} alignItems="center" >
                             <Grid item xs={3}>
                                    <TextField id="label" label="Label" variant="outlined"
                                               name={"label"}
                                               value={selectedRoom?.label || ''}
                                               onChange={handleChange}/>
                             </Grid>
                             <Grid item xs={3}>
                                 <FormControl variant={'outlined'}>
                                     <InputLabel id="inventory">Inventory</InputLabel>
                                     <Select
                                        labelid={'inventory'}
                                        id={'inventory'}
                                        label={'inventory'}
                                        name={'inventory'}
                                        value={selectedRoom?.inventory?.id || ''}
                                        onChange={handleInventoryChange} >

                                        {inventories?.map((row,i) => {
                                            return <MenuItem key={row.id} value={row.id}>{row.symbol}</MenuItem>
                                        })}
                                     </Select>
                                 </FormControl>
                             </Grid>
                         </Grid>
                    </FormControl>
                </Box>
            </Paper>
        </form>
    </div>

};

const mapStateToProps = state => ({
    room: state.Room.room,
    inventories: state.Inventory.inventories,
});

const mapDispatchToProps = (dispatch, props) => ({
    loadRoom: _ => dispatch(ROOM_ACTION.loadRoom(props.match.params.id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RoomEdit);