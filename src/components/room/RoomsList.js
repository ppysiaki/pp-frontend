import React, {useEffect, useMemo, useState} from "react";
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Link, useHistory} from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Fab from "@material-ui/core/Fab";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import {ROOM_ACTION} from "../../redux/actions/room";
import {connect} from "react-redux";
import {loadUsers} from "../../redux/actions/users";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
}));

const RoomsList = ({rooms, deleteRoom, loadRooms}) => {
    const classes = useStyles();
    const [pageTitle] = useState("Rooms list");

    useEffect(() => {
        loadRooms();
    }, []);

    const itemEditPath = (room) => `/rooms/${room.id}/edit`;
    const createUserLink = () => `/rooms/create`;
    const goToRoomInventory = (inventoryId) => `/inventory/${inventoryId}/edit`;

    const handleDelete = (room) => {
        deleteRoom(room.id);
    }

    return <div>
        <Box m={2}>
            <div className={classes.flexEvenly}>
                <Typography variant={"h2"}>{pageTitle}</Typography>
                <Fab variant={"extended"} href={createUserLink()} size={"small"} type={"button"}>
                    <AddCircleIcon />{' '}New
                </Fab>
            </div>
        </Box>
        <Paper elevation={1}>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                Id
                            </TableCell>
                            <TableCell>
                                Label
                            </TableCell>
                            <TableCell>
                                Actions
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rooms.map((row, i) => {
                            return (
                                <TableRow key={i}>
                                    <TableCell>{row.id}</TableCell>
                                    <TableCell>{row.label}</TableCell>
                                    <TableCell>
                                        <Link to={itemEditPath(row)} >
                                            <EditIcon/>
                                        </Link>
                                        {/*<Link to={{pathname:goToRoomInventory(row.inventory.id), state: {selectedObject: row}}} >*/}
                                        {/*    <LibraryBooksIcon />*/}
                                        {/*</Link>*/}

                                        <Link to={'#'} onClick={ _ => handleDelete(row)}>
                                            <DeleteForeverIcon  />
                                        </Link>


                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    </div>

}
const mapStateToProps = state => ({
    rooms: state.Room.rooms,
});

const mapDispatchToProps = dispatch => ({
    loadRooms: _ => dispatch(ROOM_ACTION.loadRooms()),
    deleteRoom: (roomId) => dispatch(ROOM_ACTION.deleteRoom(roomId)),
});


export default connect(mapStateToProps, mapDispatchToProps)(RoomsList);