class Room {
    id;
    label;
    inventory;

    constructor(id, label, inventory) {
        this.id = id;
        this.label = label;
        this.inventory = inventory;
    }

    static createEmpty() {
        let room = new Room();
        room.id = null;
        room.label = '';
        room.inventory = null;

        return room;
    }

    static createWithLabel (label) {
        const room = Room.createEmpty();
        room.label = label;

        return room;
    }
}

export default Room;