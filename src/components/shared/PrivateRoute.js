import React from "react";
import {Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";

const PrivateRoute = ({currentUser, ...rest}) => (
    !currentUser ? (<Redirect to={{pathname: '/login', state: {from: rest.location}}}/>)
        : <Route {...rest}/>
);

const mapStateToProps = store => ({
    currentUser: store.Authentication.user
});

export default connect(mapStateToProps)(PrivateRoute);