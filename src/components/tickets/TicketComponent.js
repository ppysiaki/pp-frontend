import React, {useEffect, useState} from "react";
import {Route, Switch} from "react-router-dom"
import TicketList from "./TicketList";
import TicketEdit from "./TicketEdit";
import {connect} from "react-redux";
import {fetchTickets, updateTickets, confirmTicket, doneTicket} from "../../redux/actions/Tickets"

const TicketComponent = ({data, fetchTickets, updateTickets}) => {
    const [selected, setSelected] = useState(null);

    useEffect(() => {
        fetchTickets();
    }, []);

    return <div>
        <Switch>
            <Route path="/tickets/:id/edit"
                   render={props => <TicketEdit {...props} selected={selected} onSubmit={updateTickets}/>}/>
            <Route path="/tickets"
                   render={props => <TicketList {...props} data={data} onSelect={setSelected}/>}/>
        </Switch>
    </div>
}

const mapStateToProps = state => ({
    data: state.Tickets.tickets
});

const mapDispatchToProps = dispatch => ({
    fetchTickets: () => dispatch(fetchTickets()),
    updateTickets: ticket => dispatch(updateTickets(ticket)),
    confirmTicket: ticket => dispatch(confirmTicket(ticket)),
    doneTicket: ticket => dispatch(doneTicket(ticket))
});

export default connect(mapStateToProps, mapDispatchToProps)(TicketComponent);