import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import PropTypes from 'prop-types';
import {Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Link} from "react-router-dom";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import SaveIcon from '@material-ui/icons/Save';
import FormControl from "@material-ui/core/FormControl";
import {makeStyles} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Ticket from "./model/Ticket";
import Fab from "@material-ui/core/Fab";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
}));

const TicketEdit = ({selected, onSubmit}) => {
    const classes = useStyles();
    const [pageTitle] = useState("Ticket");
    const [selectedItem, setSelectedItem] = useState(selected);
    const history = useHistory();

    const handleChange = e => {
        const {name, value} = e.target;
        setSelectedItem({...selectedItem, [name]: value});
    };

    const errorState = null;
    const [error, setError] = useState(errorState);

    const handleError = errorResponse => {
        setError(errorResponse)
    }

    const handleSubmit = e => {
        e.preventDefault();
        const copy = Object.assign({}, selected);
        for (let key of Object.keys(selectedItem)) {
            copy[key] = selectedItem[key];
        }
        let result = onSubmit(copy)
        if (result) {
            result.then(
                _ => history.goBack()
            ).catch(
                handleError
            );
        } else {
            history.goBack()
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <Box m={2}>
                    <Typography variant={"h2"}><span>{pageTitle} - {selectedItem?.id}</span></Typography>
                    <div className={classes.flexEvenly}>
                        <Typography>
                            <Link to={"/tickets"} className={classes.alignCenter}>
                                <ArrowBackIcon/> Go back
                            </Link>
                        </Typography>
                        <Fab variant={"extended"} size={"small"} type={"submit"}>
                            <SaveIcon/>{' '}Save
                        </Fab>
                    </div>
                </Box>
                <Paper elevation={3}>
                    <Box p={2}>
                        <FormControl>
                            <TextField id="priority" label="Priority" variant="outlined"
                                       name={"priority"}
                                       required={true}
                                       value={selectedItem?.priority}
                                       helperText={error}
                                       error={error}
                                       onChange={handleChange}/>
                        </FormControl>
                    </Box>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell><span>Room</span></TableCell>
                                    <TableCell><span>Priority</span></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    <TableCell><span>{selectedItem?.room.id}</span></TableCell>
                                    <TableCell><span>{selectedItem?.priority}</span></TableCell>
                                </TableRow>
                            </TableBody>

                        </Table>
                    </TableContainer>
                </Paper>
            </form>
        </div>
    )
}

TicketEdit.propTypes = {
    onSubmit: PropTypes.func,
    selected: PropTypes.instanceOf(Ticket)
}

export default TicketEdit;