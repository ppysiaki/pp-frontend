import React, {useState} from "react";
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import {Link} from "react-router-dom";
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import Checkbox from '@material-ui/core/Checkbox';
import {connect} from "react-redux";
import {confirmTicket, doneTicket} from "../../redux/actions/Tickets";

const TicketList = ({tickets, selected, onSelect, doneTicket, confirmTicket}) => {
    const [pageTitle] = useState("Tickets");
    const [selectedItem] = useState(selected);

    const handleDoneTicket = (row, state) => {
        doneTicket(row, state);
    }
    const handleConfirmTicket = (row, state) => {
        confirmTicket(row, state);
    }
    const ticketEditPath = ticket => `/tickets/${ticket.id}/edit`;

    return <div>
        <Box m={2}>
            <Typography variant={"h2"}>{pageTitle}</Typography>
        </Box>
        <Paper elevation={1}>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell><span>ID</span></TableCell>
                            <TableCell><span>Room</span></TableCell>
                            <TableCell><span>Element</span></TableCell>
                            <TableCell><span>Priority</span></TableCell>
                            <TableCell><span>Description</span></TableCell>
                            <TableCell><span>Notification date</span></TableCell>
                            <TableCell><span>Confirmed</span></TableCell>
                            <TableCell><span>Acceptation date</span></TableCell>
                            <TableCell><span>Done</span></TableCell>
                            <TableCell><span>Execution date</span></TableCell>
                            <TableCell><span>Rate</span></TableCell>
                            <TableCell><span>Comment</span></TableCell>
                            <TableCell><span>Action</span></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tickets.map((row, i) => {
                            return (
                                <TableRow key={i}>
                                    <TableCell><span>{row.id}</span></TableCell>
                                    <TableCell><span>{row.room.id}</span></TableCell>
                                    <TableCell><span>{row.inventoryElement?.name}</span></TableCell>
                                    <TableCell><span>{row.priority}</span></TableCell>
                                    <TableCell><span>{row.description}</span></TableCell>
                                    <TableCell><span>{row.notificationDate}</span></TableCell>
                                    <TableCell>
                                        <Checkbox
                                            id={selectedItem?.id.toString()}
                                            defaultChecked={selectedItem?.isConfirmed}
                                            onChange={(e) => handleConfirmTicket(row, e.target.checked)}
                                            inputProps={{'aria-label': 'primary checkbox'}}/>
                                    </TableCell>
                                    <TableCell><span>{row.acceptationDate}</span></TableCell>
                                    <TableCell>
                                        <Checkbox
                                            id={selectedItem?.id.toString()}
                                            defaultChecked={selectedItem?.isDone}
                                            onChange={(e) => handleDoneTicket(row, e.target.checked)}
                                            inputProps={{'aria-label': 'primary checkbox'}}/>
                                    </TableCell>
                                    <TableCell><span>{row.executionDate}</span></TableCell>
                                    <TableCell><span>{row.rate}</span></TableCell>
                                    <TableCell><span>{row.comment}</span></TableCell>
                                    <TableCell>
                                        <Link to={ticketEditPath(row)} onClick={() => onSelect(row)}>
                                            <EditIcon/>
                                        </Link>
                                    </TableCell>

                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    </div>;
}

const mapStateToProps = state => ({
    tickets: state.Tickets.tickets,
});

const mapDispatchToProps = dispatch => ({
    confirmTicket: (ticket, state) => dispatch(confirmTicket(ticket, state)),
    doneTicket: (ticket, state) => dispatch(doneTicket(ticket, state))
});

export default connect(mapStateToProps, mapDispatchToProps)(TicketList);
