class Ticket{
    id;
    inventoryElement;
    room;
    description;
    isDone;
    comment;
    isConfirmed;
    priority; 
    notificationDate;
    acceptationDate; 
    executionDate;
    rate;

    constructor(id, inventoryElement, room, description, isDone, comment, isConfirmed, priority, notificationDate, acceptationDate, executionDate, rate) {
        this.id = id;
        this.inventoryElement = inventoryElement;
        this.room = room;
        this.description = description;
        this.isDone = isDone;
        this.comment = comment;
        this.isConfirmed = isConfirmed;
        this.priority = priority;
        this.notificationDate = notificationDate;
        this.acceptationDate = acceptationDate;
        this.executionDate = executionDate;
        this.rate = rate;
    }

}

export default Ticket;