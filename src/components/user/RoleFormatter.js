
const ROLE_NAME_PREFIX = 'ROLE_';

const RoleFormatter = {
    denormalize: (roleName) => {
        return roleName.replace(ROLE_NAME_PREFIX, '');
    }
}

export default RoleFormatter