class User {
    token;
    username;

    constructor(token, username) {
        this.token = token;
        this.username = username;
    }

    static login(username, token) {
        return new User(token, username);
    }

    static async logout(requester) {
        return requester();
    }

    static fromObject(userObject) {
        return new User(userObject.token, userObject.username);
    }
}

export default User;