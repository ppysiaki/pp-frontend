import {Box, Paper} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import React, {useState} from "react";
import {USER_SERVICE} from "../../services/UserService";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import {useHistory} from "react-router-dom";


const UserChangePasswordForm = ({onSubmit}) => {

    let data = {
        'oldPassword': null,
        'newPassword': null,
    }

    const errorMessage = null;

    const [error, setError] = useState(errorMessage);

    const user = USER_SERVICE.get();
    const history = useHistory();

    const handleSubmit = e => {
        e.preventDefault();
        onSubmit(data.oldPassword, data.newPassword)
            .then(response => {
                USER_SERVICE.logout().then( _ => history.goBack())
            })
            .catch(error => setError('Wrong password'))
    }

    const handleChange = e => {
        const {name, value} = e.target;
        data = {...data, [name]: value};
    }

    return <div>
        <Box m={2}>
            <Typography variant={"h2"}><span>Change password - {user.username}</span></Typography>
        </Box>
        <Paper elevation={1}>
            <Box p={2}>
                <form onSubmit={handleSubmit}>
                    <FormControl>

                        <TextField id="oldPassword"
                                   label="oldPassword"
                                   name="oldPassword"
                                   variant="outlined"
                                   required={true}
                                   margin={'dense'}
                                   type={'password'}
                                   error={!!error}
                                   helperText={error}
                                   onChange={handleChange}

                        />
                        <TextField id="newPassword"
                                   label="newPassword"
                                   name="newPassword"
                                   variant="outlined"
                                   required={true}
                                   type={'password'}
                                   margin={'dense'}
                                   onChange={handleChange}
                        />
                        <Fab variant={"extended"} size={"small"} type={"submit"}>
                            <SaveIcon/>{' '}Save
                        </Fab>
                    </FormControl>
                </form>
            </Box>

        </Paper>
    </div>
}

export default UserChangePasswordForm;