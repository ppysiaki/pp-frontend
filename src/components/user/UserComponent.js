import React, { useEffect, useState } from "react";
import {Route, Switch} from "react-router-dom"
import UsersList from "./UsersList";
import UserEdit from "./UserEdit";
import UserChangePasswordForm from "./UserChangePasswordForm";
import {
    assignRoleToUser,
    changeUserPassword,
    createUser,
    deleteUser,
    loadUsers,
    updateUser
} from "../../redux/actions/users";
import {connect} from "react-redux";
import {loadRoles} from "../../redux/actions/role";
import {generatePassword} from "./PasswordGenerator";
import User from "./model/User";

const UserComponent = ({users = [], roles = [], loadUsers, loadRoles, updateUser, createUser, deleteUser, changeUserPassword}) => {
    const [selected, setSelected] = useState(null);
    const newUser = new User(null, null, [], generatePassword());

    useEffect(() => {
        loadUsers();
        loadRoles();
    }, []);

    return <div>
            <Switch>
                <Route path={"/user/changePassword"}
                       render={props => <UserChangePasswordForm onSubmit={changeUserPassword}/> }
                />
                <Route path="/user/create"
                       render={props => <UserEdit {...props} roles={[]} title={'Create user'} selected={newUser} refreshUsers={loadUsers} onSubmit={createUser}/>}
                />
                <Route path="/user/:id/edit"
                       render={props => <UserEdit {...props} roles={roles} title={'Edit user'} selected={selected} refreshUsers={loadUsers} onSubmit={updateUser}/>}
                />
                <Route  path="/user"
                        render={props => <UsersList {...props} users={users} onSelect={setSelected} onDelete={deleteUser}/>}
                />
            </Switch>
        </div>

}
const mapStateToProps = state => ({
    users: state.Users.users,
    roles: state.Role.roles,
});

const mapDispatchToProps = dispatch => ({
    loadUsers: () => dispatch(loadUsers()),
    loadRoles: () => dispatch(loadRoles()),
    updateUser: user => dispatch(updateUser(user)),
    createUser: user => dispatch(createUser(user)),
    deleteUser: user => dispatch(deleteUser(user)),
    changeUserPassword: (oldPassword, newPassword) => dispatch(changeUserPassword(oldPassword, newPassword)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserComponent);