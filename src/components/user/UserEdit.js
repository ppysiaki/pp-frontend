import React, {Component, useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";
import {Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Link, useHistory} from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import User from "./model/User";
import Checkbox from '@material-ui/core/Checkbox';
import RoleFormatter from "./RoleFormatter";

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
}));

const UserEdit = ({roles, title, selected, onSubmit, refreshUsers}) => {
    const classes = useStyles();
    const pageTitle = title;
    const [selectedUser, setSelectedUser] = useState(selected);

    const errorState = null;
    const [error, setError] = useState(errorState);

    const history = useHistory();

    const handleChange = e => {
        const {name, value} = e.target;

        setSelectedUser(Object.assign(new User, {...selectedUser, [name]: value}));
    };

    const handleError = errorResponse => {
        setError(errorResponse)
    }

    const handleSubmit = e => {
        e.preventDefault();
        let result = onSubmit(selectedUser)
            if(result) {
            result.then(
                    _ => {
                        refreshUsers();
                        history.goBack();
                    }
                ).catch(
                    handleError
                );
            } else {
                history.goBack()
            }
    }


    const handleUserRoleChange = e => {
        if (!selectedUser.roles) {
            selectedUser.roles = [];
        }
        const roleId = parseInt(e.target.id);
        const hasSelectedRole = selectedUser.hasRole(roleId);

        if (hasSelectedRole) {
            selectedUser.roles.splice(selectedUser.roles.findIndex(role => {
                return role.id === roleId;
            }), 1);
        } else {
            const role = roles.find(role => role.id === roleId);
            selectedUser.roles.push(role);
        }
    }

    const passwordBlock = () => {
        return selectedUser?.password ? <TextField id="password" label="password" variant="outlined"
                          name={"password"}
                          required={true}
                          margin={'dense'}
                          value={selectedUser?.password}
                          disabled /> : '';
    }

    return  <div>
        <form onSubmit={handleSubmit}>
            <Box m={2}>
                <Typography variant={"h2"}><span>{pageTitle} - {selectedUser?.username}</span></Typography>
                <div className={classes.flexEvenly}>
                    <Typography>
                        <Link to={"/user"} className={classes.alignCenter}>
                            <ArrowBackIcon/> Go back
                        </Link>
                    </Typography>
                    <Fab variant={"extended"} size={"small"} type={"submit"}>
                        <SaveIcon/>{' '}Save
                    </Fab>
                </div>
            </Box>
            <Paper elevation={1}>
                <Box p={2}>
                    <FormControl>
                        <TextField id="name" label="username" variant="outlined"
                                   name={"username"}
                                   required={true}
                                   value={selectedUser?.username || ''}
                                   helperText={error}
                                   error={error}
                                   onChange={handleChange}/>
                        {passwordBlock()}

                    </FormControl>
                </Box>
                {roles.length ? (                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell><span>Role</span></TableCell>
                                <TableCell><span>Has Role?</span></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {roles.map((row, i) => {
                                return <TableRow key={i}>
                                    <TableCell><span>{RoleFormatter.denormalize(row.name)}</span></TableCell>
                                    <TableCell>
                                        <Checkbox
                                            id={row.id.toString()}
                                            defaultChecked={selectedUser?.hasRole(row.id) || false}
                                            onChange={handleUserRoleChange}
                                            inputProps={{ 'aria-label': 'primary checkbox' }} />
                                    </TableCell>
                                </TableRow>

                            })}
                        </TableBody>
                    </Table>
                </TableContainer>) : ('')}

            </Paper>
        </form>
    </div>

};

UserEdit.propTypes = {
    onSubmit: PropTypes.func,
    selected: PropTypes.instanceOf(User),
    roles: PropTypes.instanceOf(Array),
}

export default UserEdit;