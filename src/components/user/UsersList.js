import React, {useState} from "react";
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Link} from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Fab from "@material-ui/core/Fab";import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(2)
        }
    },
    alignCenter: {
        display: "flex",
        alignItems: "center"
    },
    flexEvenly: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
}));

const UsersList = ({users, onSelect, onDelete}) => {
    const classes = useStyles();
    const [pageTitle] = useState("User");

    const itemEditPath = (userElement) => `/user/${userElement.id}/edit`;
    const createUserLink = () => `/user/create`;

    return <div>
        <Box m={2}>
            <div className={classes.flexEvenly}>
            <Typography variant={"h2"}>{pageTitle}</Typography>
                    <Fab variant={"extended"} href={createUserLink()} size={"small"} type={"button"}>
                        <AddCircleIcon />{' '}New
                    </Fab>
                </div>
        </Box>
        <Paper elevation={1}>
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            Id
                        </TableCell>
                        <TableCell>
                            Username
                        </TableCell>
                        <TableCell>
                            Actions
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {users.map((row, i) => {
                        return (
                            <TableRow key={i}>
                                <TableCell>{row.id}</TableCell>
                                <TableCell>{row.username}</TableCell>
                                <TableCell>
                                    <Link to={itemEditPath(row)} onClick={ _ => onSelect(row)}>
                                        <EditIcon/>
                                    </Link>

                                    <DeleteForeverIcon onClick={ _ => onDelete(row)} />

                                </TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    </Paper>
</div>

}

export default UsersList;