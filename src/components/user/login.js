import React, {useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {Card, Container} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import {connect} from "react-redux";
import {USER_ACTIONS} from "../../redux/actions/Authentication";
import {useHistory} from "react-router-dom";
import {useSnackbar} from "notistack";

const Login = ({login, errors, clearError}) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();
    const {enqueueSnackbar} = useSnackbar();

    const handleSubmit = e => {
        e.preventDefault();
        login({username, password})
            .then(() => history.push('/'));
    };

    const handleUsername = e => {
        setUsername(handleChange(e));
    };

    const handlePassword = e => {
        setPassword(handleChange(e));
    };

    const handleChange = e => {
        return e.target.value;
    };

    useEffect(() => {
        if (errors) {
            enqueueSnackbar(errors.error, {
                variant: "error",
                onClose: clearError
            });
        }
    }, [errors]);

    return (
        <Container maxWidth={"sm"}>
            <Card>
                <Box m={5}>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form noValidate onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            value={username}
                            onChange={handleUsername}
                            id="username"
                            label="User name"
                            name="username"
                            autoComplete="username"
                            autoFocus/>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            value={password}
                            onChange={handlePassword}
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"/>
                        {/*<FormControlLabel*/}
                        {/*    control={<Checkbox value="remember" color="primary"/>}*/}
                        {/*    label="Remember me"/>*/}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary">
                            Sign In
                        </Button>
                    </form>
                </Box>
            </Card>
        </Container>
    );
};

const mapStateToProps = store => ({
    errors: store.Authentication.errors
});

const mapDispatchToProps = dispatch => ({
    login: ({username, password}) => dispatch(USER_ACTIONS.login(username, password)),
    clearError: () => dispatch(USER_ACTIONS.clearError())
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);