class User {
    id;
    username;
    roles;
    password;

    constructor(id, username, roles, password) {
        this.id = id;
        this.username = username;
        this.roles = roles
        this.password = password;
    }

    hasRole (roleId) {
        return this.roles.some(role => role.id === roleId);
    }
}

export default User