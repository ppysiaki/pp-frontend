const config = {
    apiUrl: 'http://localhost:8080',
    api: {
        authentication: {
            get login() {
                return `${config.apiUrl}/user/login`
            },
            get me() {
                return `${config.apiUrl}/me`
            }
        },
        inventory: {
            get list() {
                return `${config.apiUrl}/inventories`
            },
            get inventory() {
                return `${config.apiUrl}/inventory/:id`;
            },
            get unlink() {
                return `${config.apiUrl}/inventory/:id/inventory-element/:inventoryElementId`
            }
        },
        inventory_element: {
            get list() {
                return `${config.apiUrl}/inventory-elements`
            },
            get unassignedLine() {
                return `${config.apiUrl}/inventory-elements/unassigned`
            },
            get element() {
                return `${config.apiUrl}/inventory-element/:id`;
            }
        },
        tickets: {
            get list() {
                return `${config.apiUrl}/tickets`
            },
            get ticket() {
                return `${config.apiUrl}/ticket/:id`;
            },
            get create() {
                return `${config.apiUrl}/tickets`;
            },
            get confirm() {
                return `${config.apiUrl}/ticket/:id/confirm`;
            },
            get done() {
                return `${config.apiUrl}/ticket/:id/done`;
            },
            get dismiss() {
                return `${config.apiUrl}/ticket/:id/dismiss`;
            },
            get notdone() {
                return `${config.apiUrl}/ticket/:id/not-done`;
            }
        }
    }
};

export default config;