import React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";

import App from "./App";
import store from "./redux/store";
import CssBaseline from "@material-ui/core/CssBaseline";
import {SnackbarProvider} from "notistack";

ReactDOM.render(
    <Provider store={store}>
        <CssBaseline/>
        <SnackbarProvider maxSnack={8}>
            <App/>
        </SnackbarProvider>
    </Provider>,
    document.getElementById('root')
);