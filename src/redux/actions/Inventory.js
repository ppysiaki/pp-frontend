import InventoryService from "../../services/InventoryService";
import {INVENTORY} from "../constants/actionTypes";

export function removeInventory(inventory) {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.removeInventory(inventory)
            .then(response => okOrThrow(response, () => dispatch(successRemoveInventory(inventory))))
            .catch(response => {
                dispatch(error(response));
                return Promise.reject();
            })
    }
}

export function fetchInventories() {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.fetchInventories()
            .then(response => dispatch(success(response)));
    }
}

export function updateInventory(inventory) {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.saveInventory(inventory)
            .then(response => okOrThrow(response, () => dispatch(successSaveInventory(inventory))))
            .catch(response => {
                dispatch(error(response));
                return Promise.reject();
            });
    }
}

export function unlinkInventoryElement(inventory, inventoryElement) {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.unlinkInventoryElement(inventory, inventoryElement)
            .then(response => okOrThrow(response, () => dispatch(successUnlink({inventory, inventoryElement}))))
            .catch(response => {
                dispatch(error(response));
                return Promise.reject();
            });
    }
}

export function linkInventoryElement(inventory, inventoryElement) {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.linkInventoryElement(inventory, inventoryElement)
            .then(response => okOrThrow(response, () => dispatch(successLink({inventory, inventoryElement}))))
            .catch(response => {
                dispatch(error(response));
                return Promise.reject();
            });
    }
}

export function getInventory(inventoryId) {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.fetchInventory(inventoryId)
            .then(response => response);
    }
}

export function resetLoadedInventory() {
    return function (dispatch) {
        dispatch({type: INVENTORY.CLEAR, payload: null});
    }
}

export function clearError() {
    return function (dispatch) {
        dispatch(({type: INVENTORY.ERROR.CLEAR, payload: []}));
    }
}

export function loadInventoryElements() {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.fetchInventoryElements()
            .then(response => okOrThrow(response, data => dispatch(successFetchElements(data))))
    }
}

export function assignInventoryElement(inventory, inventoryElement) {
    return function (dispatch) {
        return dispatch(({type: INVENTORY.ASSIGN_ELEMENT, payload: {inventory, inventoryElement}}));
    }
}

export function createInventory(inventory) {
    return function (dispatch) {
        dispatch(pending({}));

        return InventoryService.createInventory(inventory)
            .then(response => okOrThrow(response, data => dispatch(successCreateElement(data))))
    }
}

const okOrThrow = (response, callback) => {
    if (response.ok) {
        response.text().then(data => {
            if (data) {
                data = JSON.parse(data);
            }
            callback(data);
        });
    } else {
        return response.json()
            .then(error => {
                throw error;
            });
    }
};

const error = data => ({type: INVENTORY.ERROR.THROWN, payload: data});
const pending = data => ({type: INVENTORY.PENDING, payload: data});
const success = data => ({type: INVENTORY.FETCH, payload: data});
const successSaveInventory = data => ({type: INVENTORY.SAVE, payload: data});
const successRemoveInventory = data => ({type: INVENTORY.REMOVE, payload: data});
const successUnlink = data => ({type: INVENTORY.UNLINK, payload: data});
const successLink = data => ({type: INVENTORY.LINK, payload: data});

const successFetch = data => ({type: INVENTORY.FETCH_SINGLE, payload: data});
const successFetchElements = data => ({type: INVENTORY.FETCH_INVENTORY_ELEMENTS, payload: data});

const successCreateElement = data => ({type: INVENTORY.CREATE, payload: data});