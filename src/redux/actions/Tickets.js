import TicketService from "../../services/TicketService";
import {TICKET} from "../constants/ticketTypes";


export function fetchTickets() {
    const success = data => ({type: TICKET.FETCH, payload: data});

    return function (dispatch) {
        dispatch(pending({}));

        return TicketService.fetchTickets()
            .then(response => {
                dispatch(success(response));
            });
    }
}

export function updateTickets(ticket) {
    const successSaveTicket = data => ({type: TICKET.SAVE, payload: data});

    return function (dispatch) {
        dispatch(pending({}));

        return TicketService.saveTicket(ticket)
            .then(response => {
                dispatch(successSaveTicket(ticket));
            })
    }
}


export function confirmTicket(ticket, state) {
    const successTicketConfirmed = data => ({type: TICKET.CONFIRMED, payload: data});

    return function(dispatch){
        dispatch(pending({}));

        return TicketService.confirmTicket(ticket, state)
            .then(response => {
                dispatch(successTicketConfirmed(ticket));
            })
    }
}

export function doneTicket(ticket, state) {
    const successTicketDone = data => ({type: TICKET.DONE, payload: data});

    return function(dispatch){
        dispatch(pending({}));

        return TicketService.doneTicket(ticket, state)
            .then(response => {
                dispatch(successTicketDone(ticket));
            })
    }
}

export function createTicket(ticket) {
    const success = () => ({type: TICKET.CREATE})

    return function (dispatch) {
        dispatch(pending({}));

        return TicketService.createTicket(ticket)
            .then(response => {
                dispatch(success());
            })
    }
}

const pending = data => ({type: TICKET.PENDING, payload: data});




