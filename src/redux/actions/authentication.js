import {AUTHENTICATION} from "../constants/actionTypes";
import {USER_SERVICE} from "../../services/UserService";
import User from "../../components/user/User";

export const USER_ACTIONS = {
    login,
    logout,
    clearError
};

function clearError() {
    return dispatch => dispatch(({type: AUTHENTICATION.CLEAR_ERROR, payload: null}));
}

function login(username, password) {
    const request = user => ({type: AUTHENTICATION.LOGIN_REQUEST, payload: user});
    const success = user => ({type: AUTHENTICATION.LOGIN_SUCCESS, payload: user});
    const failure = error => ({type: AUTHENTICATION.LOGIN_FAILURE, payload: error});

    return dispatch => {
        dispatch(request({username}));

        return USER_SERVICE.login(username, password)
            .then(response => {
                if (response.ok) {
                    response.text()
                        .then(token => {
                            dispatch(success(User.login(username, token)));
                        });
                } else {
                    return response.json()
                        .then(error => {
                            throw error;
                        });
                }
            })
            .catch(response => {
                dispatch(failure(response));
                return Promise.reject();
            });
    };
}

function logout() {
    const logout = () => ({type: AUTHENTICATION.LOGOUT});
    const success = () => ({type: AUTHENTICATION.LOGOUT_SUCCESS});

    return dispatch => {
        dispatch(logout());

        return User.logout(USER_SERVICE.logout)
            .then(() => dispatch(success()));
    };

}