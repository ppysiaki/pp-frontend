import {UPDATE_NAME} from "../constants/actionTypes";

export function changeName(payload) {
    return {type: UPDATE_NAME, payload};
}