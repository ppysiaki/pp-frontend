import RoleService from "../../services/RoleService";
import {ROLE} from "../constants/roleTypes";

export function loadRoles() {

    return function (dispatch) {
        dispatch(pending({}));

        return RoleService.loadRoles()
            .then(roles => {
                    dispatch(loading(roles))
                }
            )
    }
}

const pending = data => ({type: ROLE.PENDING, payload: data})
const loading = data => ({type: ROLE.LOADING, payload: data})
