import {ROOM} from "../constants/roomTypes";
import {RoomService} from "../../services/RoomService";

export const ROOM_ACTION = {
    loadRooms,
    deleteRoom,
    loadRoom,
    updateRoom,
    createRoom,
}

function loadRooms() {
    const success = rooms => ({type: ROOM.LOAD_ROOMS, payload: rooms})
    return function (dispatch) {
        dispatch(pending());

        return RoomService.loadRooms()
            .then(rooms => {
                dispatch(success(rooms))
            })
    }
}

function deleteRoom(roomId) {
    const success = roomId => ({type: ROOM.DELETE_ROOM, payload: roomId});

    return function (dispatch) {
        dispatch(pending());

        return RoomService.deleteRoom(roomId)
            .then(response => {
                dispatch(success(roomId));
            });
    }
}

function updateRoom(room) {
    const success = _ => ({type: ROOM.UPDATE_ROOM});

    return function (dispatch) {
        dispatch(pending());

        return RoomService.updateRoom(room)
            .then(response => {
                dispatch(success());
            })
    }
}

function loadRoom(id) {
    const success = room => ({type: ROOM.LOAD_ROOM, payload: room});

    return function (dispatch) {
            dispatch(pending());

            return RoomService.loadRoom(id)
                .then(room => {
                    dispatch(success(room));
                })
    }
}

function createRoom(room) {
    const success = () => ({type: ROOM.CREATE_ROOM});

    return function (dispatch) {
        dispatch(pending());

        return RoomService.createRoom(room)
            .then(response => {
                dispatch(success())
            })
    }
}

const pending = _ => ({type: ROOM.PENDING})
