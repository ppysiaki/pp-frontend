import {USER} from "../constants/userTypes";
import UsersService from "../../services/UsersService";
import {USER_SERVICE} from "../../services/authenticationService";

export function loadUsers() {
    return function (dispatch) {
        dispatch(pending({}));

        return UsersService.loadUsers()
            .then(users => {
                dispatch(success(users));
            })
    }
}

export function updateUser(user) {
    return function (dispatch) {
        dispatch(pending({}));

        return UsersService.updateUser(user)
            .then( _ => {
                dispatch(updated({}));
            })

    }
}

export function createUser(user) {
    return function (dispatch) {
        dispatch(pending());

        return UsersService.createUser(user)
            .then( _ => {
                dispatch(created(user));
                dispatch(loadUsers());
            })
    }
}

export function deleteUser(user) {
    return function (dispatch) {
        dispatch(pending({}));

        return UsersService.deleteUser(user.id)
            .then( _ => {
                dispatch(deleted());
                dispatch(loadUsers());
            })
    }
}

export function assignRoleToUser(userId, roleId) {
    return function (dispatch) {
        dispatch(pending({}));

        return UsersService.assignRoleToUser(userId, roleId)
            .then(_ => {
                dispatch(assigend())
            })
    }
}

export function changeUserPassword(oldPassword, newPassword) {
    return function (dispatch) {
        dispatch(pending({}));

        return UsersService.changePassword(oldPassword, newPassword)
            .then( response => {
                dispatch(passwordChanged([]))

                return response;
            })
    }
}

const pending = data => ({type: USER.PENDING, payload: data})
const success = data => ({type: USER.LOAD_USERS, payload: data})
const updated = data => ({type: USER.UPDATE, payload: data})
const created = data => ({type: USER.CREATE, payload: data})
const deleted = data => ({type: USER.DELETE, payload: data})
const assigend = data => ({type: USER.ASSIGN_ROLE, payload: data})
const passwordChanged = data => ({type: USER.CHANGE_PASSWORD, payload: data})
