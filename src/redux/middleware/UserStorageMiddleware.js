import {AUTHENTICATION} from "../constants/actionTypes";
import {UserSessionStorage} from "../../services/adapter/UserSessionStorage";

const UserStorageMiddleware = store => next => action => {
    switch(action.type) {
        case AUTHENTICATION.LOGIN_SUCCESS:
            UserSessionStorage.set(action.payload);
            break;
        case AUTHENTICATION.LOGOUT_SUCCESS:
            UserSessionStorage.unset();
            break;
    }

    next(action);
};

export default UserStorageMiddleware;