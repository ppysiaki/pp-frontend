import {AUTHENTICATION} from "../constants/actionTypes";
import {USER_SERVICE} from "../../services/UserService";

const user = USER_SERVICE.get();
const initialState = {loggedIn: !!user, user, errors: null};

function Authentication(state = initialState, action) {
    switch (action.type) {
        case AUTHENTICATION.LOGIN_REQUEST:
            return {...state};
        case AUTHENTICATION.LOGIN_SUCCESS:
            return {...state, loggedIn: true, user: action.payload};
        case AUTHENTICATION.CLEAR_ERROR:
            return {...state, errors: null};
        case AUTHENTICATION.LOGIN_FAILURE:
            return {...state, errors: action.payload, loggedIn: false, user: null};
        case AUTHENTICATION.LOGOUT_SUCCESS:
            return {...state, loggedIn: false, user: null};
        case AUTHENTICATION.LOGOUT:
            return {...state};
        default:
            return {...state};
    }
}

export default Authentication;