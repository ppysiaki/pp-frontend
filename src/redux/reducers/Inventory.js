import {INVENTORY} from "../constants/actionTypes";

const initialState = {
    inventories: [],
    inventory: null,
    errors: null,
    inventoryElements: []
};

const addInventories = (inventories, payload) => {
    let newInventories = Array.from(inventories);
    let index = inventories.findIndex(item => item.id === payload.id);
    if (index > -1) {
        newInventories[index] = payload;
    } else {
        newInventories.push(payload);
    }
    return newInventories;
}

const removeInventory = (inventories, payload) => {
    let newInventories = Array.from(inventories);
    let index = newInventories.findIndex(item => item.id === payload.id);
    if (index > -1) {
        newInventories.splice(index, 1);
    }

    return newInventories;
}

const unlinkElement = (inventories, inventory, inventoryElement) => {
    let newInventories = Array.from(inventories);
    let inventoryIndex = newInventories.findIndex(item => item.id === inventory.id);
    let index = newInventories[inventoryIndex].inventoryElementList.findIndex(listItem => listItem.id === inventoryElement.id);
    if (index > -1) {
        newInventories[inventoryIndex].inventoryElementList.splice(index, 1);
    }

    return newInventories;
};

const linkElement = (inventories, inventory, inventoryElement) => {
    let newInventories = Array.from(inventories);
    let inventoryIndex = newInventories.findIndex(item => item.id === inventory.id);
    if (inventoryIndex > -1) {
        newInventories[inventoryIndex].inventoryElementList.push(inventoryElement);
    }

    return newInventories;
};

const assignInventoryElement = (inventoryElements, inventoryElement) => {
    let newInventoryElements = Array.from(inventoryElements);
    let inventoryElementIndex = newInventoryElements.findIndex(item => item.id === inventoryElement.id);
    if (inventoryElementIndex > -1) {
        newInventoryElements.splice(inventoryElementIndex, 1);
    }

    return newInventoryElements;
};

function Inventory(state = initialState, action) {
    switch (action?.type) {
        case INVENTORY.ERROR.CLEAR:
            return {...state, errors: null};
        case INVENTORY.FETCH_INVENTORY_ELEMENTS:
            return {...state, inventoryElements: action.payload};
        case INVENTORY.FETCH:
            return {...state, inventories: action.payload};
        case INVENTORY.SAVE:
            return {...state, inventories: addInventories(state.inventories, action.payload)};
        case INVENTORY.REMOVE:
            return {...state, inventories: removeInventory(state.inventories, action.payload)};
        case INVENTORY.UNLINK:
            return {
                ...state,
                inventories: unlinkElement(state.inventories, action.payload.inventory, action.payload.inventoryElement)
            };
        case INVENTORY.LINK:
            return {
                ...state
            };
        case INVENTORY.ASSIGN_ELEMENT:
            return {
                ...state,
                inventories: linkElement(state.inventories, action.payload.inventory, action.payload.inventoryElement),
                inventoryElements: assignInventoryElement(state.inventoryElements, action.payload.inventoryElement)
            };
        case INVENTORY.FETCH_SINGLE:
            return {...state, inventory: action.payload};
        case INVENTORY.CLEAR:
            return {...state, inventory: null};
        case INVENTORY.ERROR.THROWN:
            return {...state, errors: action.payload};
        case INVENTORY.PENDING:
        default:
            return state
    }
}

export default Inventory;