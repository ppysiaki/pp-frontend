import {ROOM} from "../constants/roomTypes";

const initialState = {
    rooms: [],
    room: {},
}

function Room(state = initialState, action) {
    switch (action?.type) {
        case ROOM.LOAD_ROOM:
            return {...state, room: action.payload}
        case ROOM.LOAD_ROOMS:
            return {...state, rooms: action.payload};
        case ROOM.DELETE_ROOM:
            const newRooms = state.rooms.filter(room => room.id !== action.payload)
            return {...state, rooms: newRooms}
        case ROOM.PENDING:
        case ROOM.UPDATE_ROOM:
        default:
            return state;
    }
}

export default Room;