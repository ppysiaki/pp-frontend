import {TICKET} from "../constants/ticketTypes";


const initialState = {
    tickets: []
};


const confirmTickets = (tickets, payload) => {
    let newTickets = Array.from(tickets);
    let index = newTickets.findIndex(item => item.id === payload.id);
    if (index > -1) {
        newTickets[index].isConfirmed = payload;
    }
    else{
        newTickets.push(payload)
    }
    return newTickets;
}

const doneTickets = (tickets, payload) => {
    let newTickets = Array.from(tickets);
    let index = newTickets.findIndex(item => item.id === payload.id);
    if (index > -1) {
        newTickets[index].isDone = payload;
    }

    return newTickets;
}

function Ticket(state = initialState, action) {
    switch (action?.type) {
        case TICKET.FETCH:
            return {...state, tickets: action.payload};
        case TICKET.SAVE:
            let newTickets = Array.from(state.tickets);
            let index = state.tickets.findIndex(item => item.id === action.payload.id);
            if (index > -1) {
                newTickets[index] = action.payload;
            } else {
                newTickets.push(action.payload);
            }
            return {...state, tickets: newTickets};
        case TICKET.DONE:
            return {...state, tickets: doneTickets(state.tickets, action.payload)}
        case TICKET.CONFIRMED:
            return {...state, tickets: confirmTickets(state.tickets, action.payload)}
        case TICKET.PENDING:
        default:
            return state


    }
}

export default Ticket;
