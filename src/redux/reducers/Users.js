import {USER} from "../constants/userTypes";

const initialState = {
    users: [],
}

function Users(state = initialState, action) {
    switch (action?.type) {
        case USER.LOAD_USERS:
            return {...state, users: action.payload};
        case USER.PENDING:
        case USER.DELETE:
        case USER.ASSIGN_ROLE:
        case USER.UPDATE:
        case USER.CHANGE_PASSWORD:
        default:
            return state;
    }
}

export default Users;