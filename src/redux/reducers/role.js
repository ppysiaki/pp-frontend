import {ROLE} from "../constants/roleTypes";

const initialState = {
    roles: [],
}

function Role(state = initialState, action) {
    switch (action?.type) {
        case ROLE.LOADING:
            return {...state, roles: action.payload};
        case ROLE.PENDING:
        default:
            return state;
    }
}

export default Role;