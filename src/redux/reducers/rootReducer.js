import {UPDATE_NAME} from "../constants/actionTypes";

const initialState = {
    name: ""
};

function rootReducer(state = initialState, action) {
    if (action.type === UPDATE_NAME) {
        return Object.assign({}, state, {
            name: action.payload
        });
    }
    return state;
}

export default rootReducer;