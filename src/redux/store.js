import {combineReducers, createStore, applyMiddleware} from "redux";
import rootReducer from "./reducers/rootReducer";
import Authentication from "./reducers/Authentication";
import Inventory from "./reducers/Inventory";
import Users from "./reducers/Users";
import thunk from "redux-thunk";
import UserStorageMiddleware from "./middleware/UserStorageMiddleware";
import Role from "./reducers/role";
import Tickets from "./reducers/Tickets";
import Room from "./reducers/Room";

const store = createStore(
    combineReducers({
        rootReducer,
        Inventory,
        Authentication,
        Users,
        Role,
        Room,
        Tickets
    }),
    applyMiddleware(thunk, UserStorageMiddleware)
);

export default store;
