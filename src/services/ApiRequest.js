import {USER_SERVICE} from "./UserService";

class ApiRequest {
    static methods = {
        GET: 'GET',
        POST: 'POST',
        PUT: 'PUT',
        DELETE: 'DELETE'
    };

    static async fetch(method, url, requestParamMap, data = null) {
        url = ApiRequest.replace(url, requestParamMap);
        const requestParams = {
            method: method,
            headers: ApiRequest.defaultHeaders()
        };

        if (data) {
            requestParams.body = JSON.stringify(data);
        }

        return fetch(url, requestParams);
    }

    static async get(url, requestParamMap = {}) {
        return ApiRequest.fetch(ApiRequest.methods.GET, url, requestParamMap);
    }

    static async put(url, requestParamMap, data = null) {
        return ApiRequest.fetch(ApiRequest.methods.PUT, url, requestParamMap, data);
    }

    static async post(url, requestParamMap, data = null) {
        return ApiRequest.fetch(ApiRequest.methods.POST, url, requestParamMap, data);
    }

    static async delete(url, requestParamMap) {
        return ApiRequest.fetch(ApiRequest.methods.DELETE, url, requestParamMap);
    }

    static replace(url, params = {}) {
        Object.keys(params).forEach(key => {
            url = url.replace(new RegExp(":" + key), params[key]);
        })
        return url;
    }

    static defaultHeaders = () => Object.assign({
        'Content-Type': 'application/json'
    }, USER_SERVICE.get() ? {
        // should get it from store somehow...
        'Authorization': 'Bearer ' + USER_SERVICE.get().token
    } : {});
}

export default ApiRequest;