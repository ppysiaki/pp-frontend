export default function(object, type) {
    const mappedObject = new type;
    for (let i in mappedObject) {
        if (mappedObject.hasOwnProperty(i) && object.hasOwnProperty(i)) {
            mappedObject[i] = object[i];
        }
    }
    return mappedObject;
}