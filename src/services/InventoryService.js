import config from "../config";
import Inventory from "../components/inventory/models/Inventory";
import Automapper from "./Automapper";
import ApiRequest from "./ApiRequest";

class InventoryService {
    fetchInventories() {
        return ApiRequest
            .get(config.api.inventory.list)
            .then(response => response.json())
            .then(response => response.map(item => Automapper(item, Inventory)));
    }

    fetchInventory(inventoryId) {
        return ApiRequest
            .get(config.api.inventory.inventory, {
                id: inventoryId
            })
            .then(response => response.json())
            .then(item => Automapper(item, Inventory));
    }

    saveInventory(inventory) {
        return ApiRequest
            .put(config.api.inventory.inventory, {
                id: inventory.id
            }, inventory);
    }

    async removeInventory(inventory) {
        return ApiRequest
            .delete(config.api.inventory.inventory, {
                id: inventory.id
            });
    }

    async unlinkInventoryElement(inventory, inventoryElement) {
        const newInventoryElement = Object.assign({}, inventoryElement);

        newInventoryElement.inventoryId = null;

        return ApiRequest
            .put(config.api.inventory_element.element, {
                id: inventoryElement.id
            }, newInventoryElement);
    }

    async linkInventoryElement(inventory, inventoryElement) {
        const newInventoryElement = Object.assign({}, inventoryElement);

        newInventoryElement.inventoryId = inventory.id;

        return ApiRequest
            .put(config.api.inventory_element.element, {
                id: inventoryElement.id
            }, newInventoryElement);
    }

    async fetchInventoryElements() {
        return ApiRequest
            .get(config.api.inventory_element.unassignedLine);
    }

    async createInventory(inventory) {
        return ApiRequest
            .post(config.api.inventory.list, {}, inventory);
    }
}

export default new InventoryService();