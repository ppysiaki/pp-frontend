import Automapper from "./Automapper";
import Role from "../components/user/model/Role";
import ApiRequest from "./ApiRequest";
import {router} from "./router/roleRouter";

class RoleService {

    loadRoles() {
        return ApiRequest.get(router.getRoles())
            .then(response => response.json())
            .then(response => response.map(item => Automapper(item, Role)));
    }
}

export default new RoleService();