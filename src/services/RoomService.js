import ApiRequest from "./ApiRequest";
import {router} from "./router/roomRouter";
import Automapper from "./Automapper";
import Room from "../components/room/model/Room";

export const RoomService = {
    loadRooms,
    loadRoom,
    updateRoom,
    createRoom,
    deleteRoom,
}

function loadRooms() {
    return ApiRequest.get(router.getRooms())
        .then(response => response.json())
        .then(response => response.map(item => Automapper(item, Room)));
}

function loadRoom(id) {
    return ApiRequest.get(router.getRoom(id))
        .then(response => response.json());
}

function updateRoom(room) {
    return ApiRequest.put(router.updateRoom(room.id), [], room);
}

function createRoom(room) {
    return ApiRequest.post(router.createRoom(), [], room);
}

function deleteRoom(roomId) {
    return ApiRequest.delete(router.deleteRoom(roomId));
}

