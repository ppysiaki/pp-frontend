import config from "../config";
import Ticket from "../components/tickets/model/Ticket";
import Automapper from "./Automapper";
import ApiRequest from "./ApiRequest";
import {USER_SERVICE} from "./authenticationService";

class TicketService {
    fetchTickets() {
        return ApiRequest.get(config.api.tickets.list)
        .then(response => response.json())
        .then(response => response.map(item => Automapper(item, Ticket)));
    }

    saveTicket(ticket) {
        return ApiRequest.put(config.api.tickets.ticket, {
            id: ticket.id
        }, ticket);
    }

    createTicket(ticket) {
        return ApiRequest.post(config.api.tickets.create, [], ticket)
            .then(this.handleResponse);
    }

    confirmTicket(ticket, state) {
        if(state ===  true){
            return ApiRequest.post(config.api.tickets.confirm, {id: ticket.id});
        }
        return ApiRequest.post(config.api.tickets.dismiss, {id: ticket.id});
    }

    doneTicket(ticket, state) {
        if(state ===  true){
            return ApiRequest.post(config.api.tickets.done, {id: ticket.id});
        }
        return ApiRequest.post(config.api.tickets.notdone, {id: ticket.id});
    }

    handleResponse(response) {
        return response.text()
            .then(text => {
                if (response.status === 400) {
                    return Promise.reject(text);
                }
                if (response.status === 401) {
                    return Promise.reject('Unauthorized re-log or contact administration');
                }

                if (response.status === 403) {
                    return Promise.reject(text);
                }

                return text;
            });
    }
}

export default new TicketService();