import Automapper from "./Automapper";
import User from "../components/user/model/User";
import ApiRequest from "./ApiRequest";
import {router} from "./router/userRouter"
import {USER_SERVICE} from "./authenticationService";

class UsersService {
    loadUsers() {
        return ApiRequest.get(router.getUser())
            .then(response => response.json())
            .then(response => response.map(item => Automapper(item, User)));
    }

    updateUser(user) {
        return ApiRequest.put(router.updateUser(user.id),[], user);
    }

    createUser(user) {
        return ApiRequest.post(router.saveUser(), [] , user)
            .then(this.handleResponse);

    }

    deleteUser(userId) {
        return ApiRequest.delete(router.deleteUser(userId));
    }

    assignRoleToUser(userId, roleId) {
        const data = {
            'id': roleId,
        }

        return ApiRequest.post(router.assignRoleToUser(userId), [], data)
    }

    handleResponse (response) {
        return response.text()
            .then(text => {
                if (response.status === 400) {
                    return Promise.reject(response);
                }
                if (response.status === 401) {
                    USER_SERVICE.logout();
                    location.reload(true);

                    return Promise.reject(response);
                }

                if (response.status === 403) {
                    return Promise.reject(text);
                }

                return text;
            });
    }

    changePassword(oldPassword, newPassword) {
        let data = {};
        data.oldPassword = oldPassword;
        data.newPassword = newPassword

        return ApiRequest.post(router.changePassword(), [], data)
            .then(this.handleResponse);
    }
}

export default new UsersService();