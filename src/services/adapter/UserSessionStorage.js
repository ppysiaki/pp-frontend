export const UserSessionStorage = {
    set(user) {
        sessionStorage.setItem('user', JSON.stringify(user));
    },
    unset() {
        sessionStorage.removeItem('user');
    },
    has() {
        return sessionStorage.getItem('user') !== null;
    },
    get() {
        const user = sessionStorage.getItem('user');
        return JSON.parse(user);
    }
};