import config from "../../config";

export const router = {
    getRoles: _ => `${config.apiUrl}/role`,
}