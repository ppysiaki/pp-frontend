import config from "../../config";

export const router = {
    getRooms: _ => `${config.apiUrl}/rooms`,
    getRoom: (id) => `${config.apiUrl}/room/${id}`,
    updateRoom: (id) => `${config.apiUrl}/room/${id}`,
    createRoom: () => `${config.apiUrl}/rooms`,
    deleteRoom: (id) => `${config.apiUrl}/room/${id}`,
}