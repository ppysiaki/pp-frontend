import config from "../../config";


export const router = {
    deleteUser: userId => `${config.apiUrl}/user/${userId}`,
    saveUser: _ => `${config.apiUrl}/user`,
    getUser: _ => `${config.apiUrl}/user`,
    updateUser: userId => `${config.apiUrl}/user/${userId}`,
    assignRoleToUser: userId => `${config.apiUrl}/user/${userId}`,
    changePassword: _ => `${config.apiUrl}/me/changepassword`,
}