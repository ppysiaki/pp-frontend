import config from "../config";
import {UserSessionStorage} from "./adapter/UserSessionStorage";
import ApiRequest from "./ApiRequest";
import User from "../components/user/User";

export const USER_SERVICE = {
    login,
    logout,
    get
};

async function login(username, password) {
    return ApiRequest.post(config.api.authentication.login, {}, {username, password});
}

async function logout() {
    // implement any logic required for logging out (i.e. invalidate user token on backend)
    return null;
}

function get() {
    const user = UserSessionStorage.get();
    if (!user) {
        return null;
    }
    return User.fromObject(user);
}